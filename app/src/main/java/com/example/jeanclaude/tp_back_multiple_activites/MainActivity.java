package com.example.jeanclaude.tp_back_multiple_activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    /*
     * Exemple d'application qui empile 4 activités, et la dernière dépile tout jusqu'à MainActivity en une seule fois avec le Back.
     * en fait on lance le MainActivity avec le paramètre Intent.FLAG_ACTIVITY_CLEAR_TOP qui fait ça tout seul.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Activity2.class);
                startActivity(intent);

            }
        });
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getBoolean("retour", false))
                Toast.makeText(this, "retour depuis une autre activité", Toast.LENGTH_LONG).show();
        } else
            Toast.makeText(this, "création pour la première fois", Toast.LENGTH_LONG).show();


    }
}
